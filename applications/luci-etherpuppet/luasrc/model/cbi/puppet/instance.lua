--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2008 Jo-Philipp Wich <xm@leipzig.freifunk.net>
Copyright 2013 Manuel Munz <freifunk at somakoma dot de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

]]--

require("luci.tools.webadmin")

m = Map("etherpuppet", translate("EtherPuppet"),
	translate("Etherpuppet is a small program for Linux that will " ..
		"create a voodoo doll for an ethernet interface. " ..
		"Configure an instance for your interface of choice on this " ..
		"machine and run Etherpuppet on your machine to create " ..
		"the doll.<br/>Everything seen by the real interface will be seen " ..
		"by the virtual one on your machine. Everything that will be " ..
		"sent to the virtual interface will be emitted by the real one."))

local s = m:section(TypedSection, "instance", "Instances")
s.addremove = true
s.anonymous = false
s:option(Flag, "enabled", translate("Enable"))


local iface = s:option(Value, "network", translate("Network"))
--[[
iface.template = "cbi/network_netlist"
iface.nocreate = true
iface.unspecified = false
]]--
iface.rmempty = true
iface.default = "wan"
luci.tools.webadmin.cbi_add_networks(iface)


local port = s:option(Value, "port", translate("Port")) 
port.datatype = "port"


return m
